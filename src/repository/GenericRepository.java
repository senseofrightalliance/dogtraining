package repository;

import java.util.List;

public interface GenericRepository {

	public void save(Object o);
	
	public Object findByPrimaryKey(long id);
	
	public List<Object> findAll();
	
	public void update(Object o);
	
	public void delete(Object o);
}
