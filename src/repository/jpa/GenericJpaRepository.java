package repository.jpa;

import java.util.List;

import javax.persistence.EntityManager;

import repository.GenericRepository;

public class GenericJpaRepository implements GenericRepository{
	
	EntityManager em;
	
	public GenericJpaRepository(EntityManager em) {
		this.em=em;
	}

	public void save(Object o) {
		em.persist(o);
	}

	public Object findByPrimaryKey(long id) {
		return em.find(Object.class, id);
	}

	public List<Object> findAll() {
		return em.createNamedQuery("findAllObjects",Object.class).getResultList();
	}

	public void update(Object o) {
		em.merge(o);
	}

	public void delete(Object o) {
		em.remove(o);
	}
}
