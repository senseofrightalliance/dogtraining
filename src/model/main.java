package model;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import repository.jpa.GenericJpaRepository;

public class main {
	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("addestramento-unit");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		
		GenericJpaRepository gjr= new GenericJpaRepository(em);
		
		//Creazione di un database di prova
		
		Cane c1 = new Cane("Jolie",2,"Rottweiler",0,"Femmina","Pedigree");
		Cane c2 = new Cane("Scilla",1,"Rottweiler",0,"Femmina","Pedigree");
		Cane c3 = new Cane("Max",5,"Pastore Tedesco",6,"Maschio","Pedigree");
		
		Padrone p = new Padrone("Clara","Bernasconi","mail","0000",new Date(),"Cesano");
		
		Responsabile r = new Responsabile("Simone");
		
		Associazione a = new Associazione("SOR", r);
		
		Addestratore add = new Addestratore("Francesco","Cariani","cariani@merda","56756","huehufh");
		
		CentroDiAddestramento c = new CentroDiAddestramento("Centro","vio grele","rvvr@vrvrv","45875",999);
		
		Corso co = new Corso("Corso di volo",new Date(),"67.89","I cani voleranno");
		
		//Padrone
		
		p.getCani().add(c1);
		p.getCani().add(c2);
		p.getCani().add(c3);
		p.assegnaPadrone();
		p.iscriviCane(c1, co);
		p.iscriviCane(c2, co);
		p.iscriviCane(c3, co);
		
		//Associazione
		
		a.aggiungiAddestratore(add);
		a.assegnaCentro(c);
		a.assegnaResponsabile(r);
		a.assegnaPadrone(p);
		
		//Centro
		
		c.assegnaResponsabile(r);
		c.aggiungiCorso(co);
		
		//Corso
		
		co.assegnaAddestratore(add);
		
		tx.begin();

		gjr.save(p);
		gjr.save(r);
		gjr.save(a);
		gjr.save(add);
		gjr.save(c);
		gjr.save(co);
		gjr.save(c1);
		gjr.save(c2);
		gjr.save(c3);
		
		tx.commit();

		em.close();
		emf.close();
	}
}
