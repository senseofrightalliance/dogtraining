package model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Cane {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	private String nome;
	private int eta;
	private String razza;
	private int livello;
	private String sesso;
	private String pedigree;
	
	@ManyToOne
	private Padrone padrone;
	
	@ManyToMany
	private List<Corso> corsi;
	
	public Cane() {} //jolie scilla max

	public Cane(String nome, int eta, String razza, int livello, String sesso, String pedigree) {
		super();
		this.nome = nome;
		this.eta = eta;
		this.razza = razza;
		this.livello = livello;
		this.sesso = sesso;
		this.pedigree = pedigree;
		this.corsi = new ArrayList<Corso>();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getEta() {
		return eta;
	}

	public void setEta(int eta) {
		this.eta = eta;
	}

	public String getRazza() {
		return razza;
	}

	public void setRazza(String razza) {
		this.razza = razza;
	}

	public int getLivello() {
		return livello;
	}

	public void setLivello(int livello) {
		this.livello = livello;
	}

	public String getSesso() {
		return sesso;
	}

	public void setSesso(String sesso) {
		this.sesso = sesso;
	}

	public String getPedigree() {
		return pedigree;
	}

	public void setPedigree(String pedigree) {
		this.pedigree = pedigree;
	}

	public Padrone getPadrone() {
		return padrone;
	}

	public void setPadrone(Padrone padrone) {
		this.padrone = padrone;
	}

	public List<Corso> getCorsi() {
		return corsi;
	}

	public void setCorsi(List<Corso> corsi) {
		this.corsi = corsi;
	}
}
