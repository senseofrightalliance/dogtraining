package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Padrone {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	private String nome;
	private String cognome;
	private String mail;
	private String telefono;
	private Date dataDiNascita;
	private String luogoDiNascita;
	
	@OneToMany
	@JoinColumn(name="padrone_id")
	private List<Cane> cani;
	
	public Padrone() {}

	public Padrone(String nome, String cognome, String mail, String telefono, Date dataDiNascita,
			String luogoDiNascita) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.mail = mail;
		this.telefono = telefono;
		this.dataDiNascita = dataDiNascita;
		this.luogoDiNascita = luogoDiNascita;
		this.cani = new ArrayList<Cane>();
	}
	
	public void assegnaPadrone() {
		for(Cane c : this.cani) {
			c.setPadrone(this);
		}
	}
	
	public void iscriviCane(Cane cane,Corso corso) {
		corso.getCani().add(cane);
		cane.getCorsi().add(corso);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Date getDataDiNascita() {
		return dataDiNascita;
	}

	public void setDataDiNascita(Date dataDiNascita) {
		this.dataDiNascita = dataDiNascita;
	}

	public String getLuogoDiNascita() {
		return luogoDiNascita;
	}

	public void setLuogoDiNascita(String luogoDiNascita) {
		this.luogoDiNascita = luogoDiNascita;
	}
	
	public List<Cane> getCani() {
		return this.cani;
	}
	
	public void setCani(List<Cane> cani) {
		this.cani=cani;
	}
}
