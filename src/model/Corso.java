package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Corso {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	private String nome;
	private Date data;
	private String ora;
	private String descrizione;
	
	@ManyToMany(mappedBy="corsi")
	private List<Cane> cani;
	
	@ManyToOne
	private Addestratore addestratore;
	
	public Corso() {}

	public Corso(String nome, Date data, String ora, String descrizione) {
		super();
		this.nome = nome;
		this.data = data;
		this.ora = ora;
		this.descrizione = descrizione;
		this.cani = new ArrayList<Cane>();
	}

	public void assegnaAddestratore(Addestratore addestratore) {
		this.addestratore = addestratore;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getOra() {
		return ora;
	}

	public void setOra(String ora) {
		this.ora = ora;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public List<Cane> getCani() {
		return cani;
	}

	public void setCani(List<Cane> cani) {
		this.cani = cani;
	}

	public Addestratore getAddestratore() {
		return addestratore;
	}
}
